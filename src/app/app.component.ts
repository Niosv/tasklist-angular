import { Component, OnInit, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterOutlet } from '@angular/router';
import { TareasService } from './services/tareas.service';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, FormsModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent implements OnInit {
  listado: string[] = [];
  newTarea: string = '';

  //inyección del servicio
  private _tareasService = inject(TareasService);

  //Obtener tareas al inicio del componente
  ngOnInit(): void {
    this.listado = this._tareasService.getTareas();
  }

  //Agregar tarea
  addTarea() {
    this._tareasService.addTarea(this.newTarea);
    this.newTarea = '';
    this.listado = this._tareasService.getTareas(); //Actualiza listado visualizado
  }

  deleteTarea(index: number) {
    this._tareasService.deleteTarea(index);
    this.listado = this._tareasService.getTareas(); //Actualiza listado visualizado
  }
}
